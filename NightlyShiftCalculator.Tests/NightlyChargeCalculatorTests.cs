using NightlyShiftCalculator.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NightlyShiftCalculator.Tests
{
    [TestClass]
    public class NightlyChargeCalculatorTests
    {
        [TestInitialize]
        public void Setup()
        {
            _calculator = new NightlyChargeCalculator();
        }

        [TestMethod]
        public void StartTimeToBedtimePayIs36Dollars()
        {
            var nightlyCharge = _calculator.Calculate(Hours.Seventeen, Hours.Twenty);
            Assert.IsTrue(36 == nightlyCharge);
        }

        [TestMethod]
        public void StartTimeToHourTwentyOnePayIs48Dollars()
        {
            var nightlyCharge = _calculator.Calculate(Hours.Seventeen, Hours.TwentyOne);
            Assert.IsTrue(48 == nightlyCharge);
        }

        [TestMethod]
        public void StartTimeToMidnightPayIs72Dollars()
        {
            var nightlyCharge = _calculator.Calculate(Hours.Seventeen, Hours.Zero);
            Assert.IsTrue(72 == nightlyCharge);
        }

        [TestMethod]
        public void BedtimeToMidnightPayIs24Dollars()
        {
            var nightlyCharge = _calculator.Calculate(Hours.TwentyOne, Hours.Zero);
            Assert.IsTrue(24 == nightlyCharge);
        }

        [TestMethod]
        public void MidnightToEndofJobPayIs64Dollars()
        {
            var nightlyCharge = _calculator.Calculate(Hours.Zero, Hours.Four);
            Assert.IsTrue(64 == nightlyCharge);
        }

        [TestMethod]
        public void FullDayPayIs164Dollars()
        {
            var nightlyCharge = _calculator.Calculate(Hours.Seventeen, Hours.Four);
            Assert.IsTrue(136 == nightlyCharge);
        }

        private static NightlyChargeCalculator _calculator;
    }
}