﻿using System;
using System.Collections.Generic;

namespace NightlyShiftCalculator.Core
{
    public class NightlyChargeCalculator
    {
        public decimal Calculate(Hours startTime, Hours endTime)
        {
            ShiftCalculater shiftOne = new ShiftOneCalculator();
            ShiftCalculater shiftTwo = new ShiftTwoCalculator();
            ShiftCalculater shiftThree = new ShiftThreeCalculator();

            shiftOne.SetSuccessor(shiftTwo);
            shiftTwo.SetSuccessor(shiftThree); ;

            var invoice = new Invoice
            {
                WorkedHours = GetWorkedHours(startTime, endTime)
            };
            shiftOne.Calculate(invoice);

            return invoice.NightlyCharge;
        }

        private List<DateTime> GetWorkedHours(Hours startTime, Hours endTime)
        {
            var t1 = startTime.Resolve().Parse();
            var t2 = endTime.Resolve().Parse();

            var times = new List<DateTime>();
            for (DateTime date = new DateTime(0001, 1, t1.ToString("tt").Equals("PM") ? 1 : 2, (int)startTime, 00, 00); date <= new DateTime(0001, 1, t2.ToString("tt").Equals("PM") ? 1 : 2, (int)endTime, 00, 00); date = date.AddHours(1))
                times.Add(date);

            return times;
        }
    }
}