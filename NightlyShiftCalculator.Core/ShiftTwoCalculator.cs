﻿using System;
using System.Linq;

namespace NightlyShiftCalculator.Core
{
    public class ShiftTwoCalculator : ShiftCalculater
    {
        public override void Calculate(Invoice invoice)
        {
            var nightlyCharge = 0m;

            var t1 = invoice.WorkedHours.Where(x => x.CompareTo(BedTime) >= 0).Select(x => x);
            var t2 = invoice.WorkedHours.Where(x => x.CompareTo(Midnight) <= 0).Select(x => x);
            var times = t1.Intersect(t2).ToList();
            if (!times.Any())
            {
                successor.Calculate(invoice);
                return;
            }

            var startTime = times.First();
            var endTime = times.Last();
            var workedHours = endTime.Subtract(startTime).Hours;
            if (workedHours == 0)
            {
                successor.Calculate(invoice);
                return;
            }

            nightlyCharge = workedHours * HourlyRate;

            Console.WriteLine($"Second shift charge: {nightlyCharge.ToString("c")}");
            invoice.NightlyCharge += nightlyCharge;
            successor.Calculate(invoice);
        }

        private const decimal HourlyRate = 8;
        private readonly DateTime BedTime = new DateTime(0001, 1, 1, (int)Hours.TwentyOne, 00, 00);
        private readonly DateTime Midnight = new DateTime(0001, 1, 2, (int)Hours.Zero, 00, 00);
    }
}