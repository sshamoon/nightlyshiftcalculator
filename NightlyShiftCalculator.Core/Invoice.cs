﻿using System;
using System.Collections.Generic;

namespace NightlyShiftCalculator.Core
{
    public class Invoice
    {
        public decimal NightlyCharge { get; set; }
        public List<DateTime> WorkedHours { get; internal set; }
    }
}