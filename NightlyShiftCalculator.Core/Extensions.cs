﻿using System;

namespace NightlyShiftCalculator.Core
{
    internal static class Extensions
    {
        public static DateTime Parse(this string date)
        {
            return DateTime.Parse(date);
        }

        public static string Resolve(this Hours hour)
        {
            switch (hour)
            {
                case Hours.Seventeen:
                    return "05:00 PM";
                case Hours.Eighteen:
                    return "06:00 PM";
                case Hours.Nineteen:
                    return "07:00 PM";
                case Hours.Twenty:
                    return "08:00 PM";
                case Hours.TwentyOne:
                    return "09:00 PM";
                case Hours.TwentyTwo:
                    return "10:00 PM";
                case Hours.TwentyThree:
                    return "11:00 PM";
                case Hours.Zero:
                    return "12:00 AM";
                case Hours.One:
                    return "01:00 AM";
                case Hours.Two:
                    return "02:00 AM";
                case Hours.Three:
                    return "03:00 AM";
                case Hours.Four:
                    return "04:00 AM";
                default:
                    return new Exception("No such hour found").Message;
            }
        }
    }
}