﻿namespace NightlyShiftCalculator.Core
{
    public abstract class ShiftCalculater
    {
        protected ShiftCalculater successor;

        public void SetSuccessor(ShiftCalculater successor)
        {
            this.successor = successor;
        }

        public abstract void Calculate(Invoice invoice);
    }
}