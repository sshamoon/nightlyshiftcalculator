﻿using System;
using System.Linq;

namespace NightlyShiftCalculator.Core
{
    public class ShiftOneCalculator : ShiftCalculater
    {
        public override void Calculate(Invoice invoice)
        {
            var t1 = invoice.WorkedHours.Where(x => x.CompareTo(StartTime) >= 0).Select(x => x);
            var t2 = invoice.WorkedHours.Where(x => x.CompareTo(BedTime) <= 0).Select(x => x);
            var times = t1.Intersect(t2).ToList();
            if (!times.Any())
            {
                successor.Calculate(invoice);
                return;
            }

            var startTime = times.First();
            var endTime = times.Last();
            var workedHours = endTime.Subtract(startTime).Hours;
            if (workedHours == 0)
            {
                successor.Calculate(invoice);
                return;
            }

            invoice.NightlyCharge = workedHours * HourlyRate;

            Console.WriteLine($"First shift charge: {invoice.NightlyCharge.ToString("c")}");
            successor.Calculate(invoice);
        }

        private const decimal HourlyRate = 12;
        private readonly DateTime StartTime = new DateTime(0001, 1, 1, (int)Hours.Seventeen, 00, 00);
        private readonly DateTime BedTime = new DateTime(0001, 1, 1, (int)Hours.TwentyOne, 00, 00);
    }
}