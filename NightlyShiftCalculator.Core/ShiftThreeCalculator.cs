﻿using System;
using System.Linq;

namespace NightlyShiftCalculator.Core
{
    public class ShiftThreeCalculator : ShiftCalculater
    {
        public override void Calculate(Invoice invoice)
        {
            var nightlyCharge = 0m;

            var t1 = invoice.WorkedHours.Where(x => x.CompareTo(Midnight) >= 0).Select(x => x);
            var t2 = invoice.WorkedHours.Where(x => x.CompareTo(EndofJob) <= 0).Select(x => x);
            var times = t1.Intersect(t2).ToList();
            if (!times.Any())
                return;

            var startTime = times.First();
            var endTime = times.Last();
            var workedHours = endTime.Subtract(startTime).Hours;
            if (workedHours == 0)
                return;

            nightlyCharge = workedHours * HourlyRate;

            Console.WriteLine($"Third shift charge: {nightlyCharge.ToString("c")}");
            invoice.NightlyCharge += nightlyCharge;
        }

        private const decimal HourlyRate = 16;
        private readonly DateTime Midnight = new DateTime(0001, 1, 2, (int)Hours.Zero, 00, 00);
        private readonly DateTime EndofJob = new DateTime(0001, 1, 2, (int)Hours.Four, 00, 00);
    }
}