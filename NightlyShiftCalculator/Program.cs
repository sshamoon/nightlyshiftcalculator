﻿using NightlyShiftCalculator.Core;
using System;

namespace NightlyShiftCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var nightlyCharge = new NightlyChargeCalculator()
                .Calculate(Hours.Seventeen, Hours.Four);
            Console.WriteLine($"Total nightly charge: {nightlyCharge}");
            Console.ReadKey();
        }
    }
}